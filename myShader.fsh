#version 330 core
out vec4 FragColor;

in vec3 objColor;
in vec2 TexCoords;

uniform sampler2D texture_diffuse0;
uniform sampler2D texture_diffuse1;
uniform float mixRatio;

void main()
{
   FragColor = mix(texture(texture_diffuse0, TexCoords),
                   texture(texture_diffuse1, TexCoords), mixRatio);
}
