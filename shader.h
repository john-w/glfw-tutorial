#ifndef SHADER_HPP
#define SHADER_HPP

#include "glad/glad.h"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader
{
    unsigned int _ID;

    public:
        Shader(const char *vertexPath, const char *fragmentPath);

        void use();
        void setBool(const std::string &name, const bool &value) const;
        void setInt(const std::string &name, const int &value) const;
        void setFloat(const std::string &name, const float &value) const;
        void setMatrix4f(const std::string &name, const glm::mat4 &matrix) const;
        void setVec3(const std::string &name, const glm::vec3 &vec) const;

        void checkCompileErrors(GLuint shader, std::string type);

        void setID(int ID);
        int getID();
};

#endif // SHADER_HPP
