#include "shader.h"

Shader::Shader(const char *vertexPath, const char *fragmentPath)
{
   std::string vertexCode;
   std::string fragmentCode;
   std::ifstream vShaderFile;
   std::ifstream fShaderFile;

   // Ensure these objects can throw exceptions
   vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
   fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

   try
   {
       vShaderFile.open(vertexPath);
       fShaderFile.open(fragmentPath);
       std::stringstream vShaderStream, fShaderStream;

       // Read the buffers
       vShaderStream << vShaderFile.rdbuf();
       fShaderStream << fShaderFile.rdbuf();

       vShaderFile.close();
       fShaderFile.close();
       // ... and convert it to strings
       vertexCode = vShaderStream.str();
       fragmentCode = fShaderStream.str();


   }
   catch (std::ifstream::failure e)
   {
       std::cout << "ERROR::SHADER::FILE_NOT_SUCCESSFULLY_READ" << std::endl;
   }

   const char* vShaderCode = vertexCode.c_str();
   const char* fShaderCode = fragmentCode.c_str();

   unsigned int vertex, fragment;

   // Create and compile Vertex shader
   vertex = glCreateShader(GL_VERTEX_SHADER);
   glShaderSource(vertex, 1, &vShaderCode, NULL);
   glCompileShader(vertex);
   checkCompileErrors (vertex, "VERTEX");

   // Create and compile Fragment shader
   fragment = glCreateShader(GL_FRAGMENT_SHADER);
   glShaderSource(fragment, 1, &fShaderCode, NULL);
   glCompileShader(fragment);
   checkCompileErrors (fragment, "FRAGMENT");

   _ID = glCreateProgram();
   glAttachShader(_ID, vertex);
   glAttachShader(_ID, fragment);
   glLinkProgram(_ID);
   checkCompileErrors (_ID, "PROGRAM");

   glDeleteShader(vertex);
   glDeleteShader(fragment);
}

void Shader::use()
{
    glUseProgram(_ID);
}

void Shader::setBool(const std::string &name, const bool &value) const
{
    glUniform1i(glGetUniformLocation(_ID, name.c_str()), (int)value);
}

void Shader::setInt(const std::string &name, const int &value) const
{
    glUniform1i(glGetUniformLocation(_ID, name.c_str()), value);
}

void Shader::setFloat(const std::string &name, const float &value) const
{
    glUniform1f(glGetUniformLocation(_ID, name.c_str()), value);
}

void Shader::setMatrix4f(const std::string &name, const glm::mat4 &matrix) const
{
    glUniformMatrix4fv(glGetUniformLocation(_ID, name.c_str()),
                       1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::setVec3(const std::string &name, const glm::vec3 &vec) const
{
    glUniform3fv(glGetUniformLocation(_ID, name.c_str()), 1, &vec[0]);
}

void Shader::checkCompileErrors(GLuint shader, std::string type)
{
    GLint success;
    GLchar infoLog[1024];
    if (type != "PROGRAM")
    {
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(shader, 1024, NULL, infoLog);
            std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type
                 << "\n" << infoLog << "\n--------------------" << std::endl;
        }
    }
    else
    {
        glGetShaderiv(shader, GL_LINK_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(shader, 1024, NULL, infoLog);
            std::cout << "ERROR::SHADER_LINKING_ERROR of type: " << type
                 << "\n" << infoLog << "\n--------------------" << std::endl;
        }

    }
}

void Shader::setID(int ID)
{
    _ID = ID;
}

int Shader::getID()
{
    return _ID;
}


