TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QT += opengl
TARGET = GLFWTutorial

INCLUDEPATH += glad

LIBS += -ldl -lglfw -lGL -lX11 -lpthread -lboost_program_options

SOURCES += main.cpp \
           glhandler.cpp \
           inputhandler.cpp \
           shader.cpp \
           texture2d.cpp \
           camera.cpp \
           glad.c \
    mesh.cpp

HEADERS += glhandler.h \
           inputhandler.h \
           shader.h \
           texture2d.h \
           glad.h \
           stb_image.h \
           camera.h \
           constants.h \
    mesh.h

OTHER_FILES += *.vsh \
               *.fsh

QMAKE_LFLAGS   += -fopenmp

DISTFILES += \
    lampshader.fsh \
    lampshader.vsh \
    lightningshader.fsh
