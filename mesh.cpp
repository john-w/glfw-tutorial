#include "mesh.h"

void Mesh::setupMesh()
{
    glGenVertexArrays(1, &_VAO);
    glGenBuffers(1, &_VBO);
    glGenBuffers(1, &_EBO);

    glBindVertexArray(_VAO);

    glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(Vertex),
                                  &_vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indices.size() * sizeof(unsigned int),
                                          &_indices[0], GL_STATIC_DRAW);

    // Vertex Positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

    // Vertex Normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                                                    (void*)offsetof(Vertex, normal));

    // Vertex Textures
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                                                    (void*)offsetof(Vertex, texCoords));

    // Vertex Tangent
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                                                    (void*)offsetof(Vertex, tangent));

    // Vertex biTangent
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                                                    (void*)offsetof(Vertex, biTangent));
    glBindVertexArray(0);
}

Mesh::Mesh(std::vector<Vertex> vertices,
           std::vector<unsigned int> indices,
           std::vector<Texture> textures)
    : _vertices(vertices), _indices(indices), _textures(textures)
{
    setupMesh ();
}


void Mesh::draw(Shader shader)
{
    // Using texture_typei convention, we can use (and properly render) textures
    // without having to create a different class each time
    unsigned int diffuseNr  = 1;
    unsigned int specularNr = 1;
    unsigned int normalNr   = 1;
    unsigned int heightNr   = 1;

    for (unsigned int i = 0; i < _textures.size(); i++)
    {
        glActiveTexture (GL_TEXTURE0 + i);

        std::string number;
        std::string name = _textures[i].type;

        if (name == "texture_diffuse") 		    number = std::to_string(diffuseNr++);
        else if (name == "texture_specular") 	number = std::to_string(specularNr++);
        else if (name == "texture_normal") 	    number = std::to_string(normalNr++);
        else if (name == "texture_height") 	    number = std::to_string(heightNr++);

        shader.setFloat((name + number).c_str(), i);
        glBindTexture(GL_TEXTURE_2D, _textures[i].ID);
    }
    glActiveTexture(GL_TEXTURE0);

    // and Draw everything
    glBindVertexArray(_VAO);
    glDrawElements (GL_TRIANGLES, _indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}
