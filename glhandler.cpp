#include "glhandler.h"

GLHandler::GLHandler(int scrWidth, int scrHeight, char* windowName)
    : _scrHeight(scrHeight), _scrWidth(scrWidth), _windowName(windowName)
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

    // glfw window creation
    _window = glfwCreateWindow(_scrWidth, _scrHeight, _windowName, NULL, NULL);
    if (_window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        exit(-1);
    }
    glfwMakeContextCurrent(_window);
    glfwSetFramebufferSizeCallback(_window, framebuffer_size_callback);

    // glad: load all OpenGL function pointers
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        exit(-1);
    }

    // This fixes the draw-order "see-through" effect
    glEnable(GL_DEPTH_TEST);

}

GLHandler::~GLHandler()
{
}

GLFWwindow *GLHandler::getWindow()
{
    return _window;
}

bool GLHandler::isClosed()
{
    return glfwWindowShouldClose(_window);
}

void GLHandler::renderBackground(float *color)
{
   glClearColor(color[0], color[1], color[2], 1.0f);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}
