#ifndef GLHANDLER_HPP
#define GLHANDLER_HPP

#include "glad/glad.h"
#include <GLFW/glfw3.h>

#include <iostream>
#include <cstdlib>

#define MESH

void framebuffer_size_callback(GLFWwindow* window, int width, int height);

class GLHandler
{
    private:
        int _scrWidth;
        int _scrHeight;
        char* _windowName;

    private:
        GLFWwindow* _window;

    public:
        GLHandler(int scrWidth, int scrHeight, char* windowName);
        ~GLHandler();

        bool isClosed();

    public:
        void renderBackground(float *color);

    public:
        GLFWwindow* getWindow();

};

#endif // GLHANDLER_HPP
