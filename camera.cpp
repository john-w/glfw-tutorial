#include "camera.h"

Camera::Camera(vec3 postion = vec3(0.0f, 0.0f, 0.0f),
               vec3 up      = vec3(0.0f, 1.0f, 0.0f),
               float yaw = YAW, float pitch = PITCH)
    : _front(vec3(0.0f, 0.0f, -1.0f)),
      _movementSpeed(SPEEDFAC), _mouseSensitivity(SENSITIVTY), _zoom(ZOOM)
{
    _position = postion;
    _worldUp = up;
    _pitch = pitch;
    _yaw = yaw;

    updateCameraVectors ();
}

void Camera::updateCameraVectors()
{
    vec3 front;
    front.x = cos(radians(_pitch)) * cos(radians(_yaw));
    front.y = sin(radians(_pitch));
    front.z = cos(radians(_pitch)) * sin(radians(_yaw));
    _front  = normalize(front);
    _right  = normalize(cross(_front, _worldUp));
    _up 	= normalize(cross(_right, _front));
}

mat4 Camera::getViewMatrix()
{
    return lookAt(_position, _position + _front, _up);
}

void Camera::processKeyboard(CameraMovement direction, float dt)
{
    float velocity = _movementSpeed * dt;

    switch (direction)
    {
        case FORWARD: 			_position += _front * velocity; break;
        case BACKWARD:			_position -= _front * velocity; break;
        case LEFT:    			_position -= _right * velocity; break;
        case RIGHT:   			_position += _right * velocity; break;
    }

//    _position.y = 0.0f;

}

void Camera::processMouseMovement(float xoffset, float yoffset,
                                  GLboolean constrainPitch = MOUSE_CONSTRAINT,
                                  GLboolean invertY = INVERT_Y)
{
    xoffset *= _mouseSensitivity;
    yoffset *= _mouseSensitivity;

    _yaw   += xoffset;
    _pitch += (invertY) ? -yoffset : yoffset;

    if (constrainPitch)
    {
        if (_pitch < -89.9) 		_pitch = -89.9;
        if (_pitch >  89.9) 		_pitch =  89.9;
    }

    updateCameraVectors ();
}

void Camera::processMouseScroll(float yoffset)
{
    if (_zoom < 1.0) 						_zoom = 1.0;
    else if (_zoom > 45.0) 					_zoom = 45.0;
    else 					 		_zoom -= yoffset;
}

float Camera::getZoom()
{
    return _zoom;
}

vec3 Camera::getPosition()
{
    return _position;
}
