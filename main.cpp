#include "glad/glad.h"
#include <GLFW/glfw3.h>

// GL transformations
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <cmath>

#include "constants.h"

#include "glhandler.h"
#include "shader.h"

#include "inputhandler.h"
#include "camera.h"

#include "texture2d.h"
#include "mesh.h"

#define MESH

static float lastX_g 		=  SCR_WIDTH/2;
static float lastY_g 		=  SCR_HEIGHT/2;

static bool firstMouse_g 	= true;

Camera *camera = new Camera(glm::vec3(3.0f, 3.0f, 4.0f),
                            glm::vec3(0.0f, 1.0f, 0.0f),
                            YAW, PITCH);


void mouse_callback(GLFWwindow *window, double xpos, double ypos);
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);

int main()
{
    // Initialize Window
    GLHandler *glHandler = new GLHandler(SCR_WIDTH, SCR_HEIGHT, "MyOwnWindow");
    GLFWwindow *window = glHandler->getWindow ();

    // Initialize Input
    InputHandler *inputHandler = new InputHandler(glHandler, camera);

    // Set up the shader
//    Shader myShader("/home/someone/Documents/dev/Qtcreator/GLFW_Tutorial/myShader.vsh",
//                    "/home/someone/Documents/dev/Qtcreator/GLFW_Tutorial/myShader.fsh");

/******************************************************************************/
    Shader *lightningShader = new Shader("/home/someone/Documents/dev/Qtcreator/GLFW_Tutorial/lampshader.vsh",
                                   "/home/someone/Documents/dev/Qtcreator/GLFW_Tutorial/lightningshader.fsh");

    Shader *lampShader = new Shader("/home/someone/Documents/dev/Qtcreator/GLFW_Tutorial/lampshader.vsh",
                                   "/home/someone/Documents/dev/Qtcreator/GLFW_Tutorial/lampshader.fsh");
/******************************************************************************/

    // Set up the Camera
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    // Set up a background
    float backgroundColor[3] = { 0.1f, 0.1f, 0.1f};

    // Set up a cube!
    std::vector<Vertex> vertices
    {
        {{ 0.5f,  0.5f,  0.5f}, { 0.0,  0.0,  1.0},  { 1.0f,  1.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{ 0.5f, -0.5f,  0.5f}, { 0.0,  0.0,  1.0},  { 1.0f,  0.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{-0.5f, -0.5f,  0.5f}, { 0.0,  0.0,  1.0},  { 0.0f,  0.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{-0.5f,  0.5f,  0.5f}, { 0.0,  0.0,  1.0},  { 0.0f,  1.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},

        // Back
        {{ 0.5f,  0.5f, -0.5f}, { 0.0,  0.0, -1.0},  { 1.0f,  1.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{ 0.5f, -0.5f, -0.5f}, { 0.0,  0.0, -1.0},  { 1.0f,  0.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{-0.5f, -0.5f, -0.5f}, { 0.0,  0.0, -1.0},  { 0.0f,  0.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{-0.5f,  0.5f, -0.5f}, { 0.0,  0.0, -1.0},  { 0.0f,  1.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},

        // Bottom
        {{ 0.5f, -0.5f,  0.5f}, { 0.0, -1.0,  0.0},  { 1.0f,  1.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{ 0.5f, -0.5f, -0.5f}, { 0.0, -1.0,  0.0},  { 1.0f,  0.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{-0.5f, -0.5f, -0.5f}, { 0.0, -1.0,  0.0},  { 0.0f,  0.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{-0.5f, -0.5f,  0.5f}, { 0.0, -1.0,  0.0},  { 0.0f,  1.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},

        // Top
        {{ 0.5f,  0.5f,  0.5f}, { 0.0,  1.0,  0.0},  { 1.0f,  1.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{ 0.5f,  0.5f, -0.5f}, { 0.0,  1.0,  0.0},  { 1.0f,  0.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{-0.5f,  0.5f, -0.5f}, { 0.0,  1.0,  0.0},  { 0.0f,  0.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{-0.5f,  0.5f,  0.5f}, { 0.0,  1.0,  0.0},  { 0.0f,  1.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},

        // Left
        {{-0.5f,  0.5f,  0.5f}, {-1.0,  0.0,  0.0},  { 1.0f,  1.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{-0.5f,  0.5f, -0.5f}, {-1.0,  0.0,  0.0},  { 1.0f,  0.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{-0.5f, -0.5f, -0.5f}, {-1.0,  0.0,  0.0},  { 0.0f,  0.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{-0.5f, -0.5f,  0.5f}, {-1.0,  0.0,  0.0},  { 0.0f,  1.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},

        // Right
        {{ 0.5f,  0.5f,  0.5f}, { 1.0,  0.0,  0.0},  { 1.0f,  1.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{ 0.5f,  0.5f, -0.5f}, { 1.0,  0.0,  0.0},  { 1.0f,  0.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{ 0.5f, -0.5f, -0.5f}, { 1.0,  0.0,  0.0},  { 0.0f,  0.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}},
        {{ 0.5f, -0.5f,  0.5f}, { 1.0,  0.0,  0.0},  { 0.0f,  1.0f}, { 0.0,  0.0,  0.0}, { 0.0,  0.0,  0.0}}
    };

    std::vector<unsigned int> indices
    {
        0 , 1 , 3 , // RFront
        1 , 2 , 3 , // LFront

        4 , 5 , 7 , // RBack
        5 , 6 , 7 , // LBack

        8 , 9 , 11, // RBottom
        9 , 10, 11, // LBottom

        12, 13, 15, // RTop
        13, 14, 15, // LTop

        16, 17, 19, // LLeft
        17, 18, 19, // RLeft

        20, 21, 23, // LRight
        21, 22, 23  // RRight
    };

    std::vector<Texture> textures
    {
//        { 1, "texture_diffuse", Texture2D("/home/someone/Documents/dev/Qtcreator/GLShaders/res/container.jpg", GL_RGB, false, 0)},
//        { 2, "texture_diffuse", Texture2D("/home/someone/Documents/dev/Qtcreator/GLShaders/res/awesomeface.png", GL_RGBA, true, 1)}
    };

    Mesh *mesh = new Mesh(vertices, indices, textures);

//    myShader.use();
//    myShader.setInt ("texture_diffuse0", 0);
//    myShader.setInt ("texture_diffuse1", 1);

    // Defining more cubes
    std::vector<glm::vec3> cubePositions = {
        glm::vec3(1.2f, 1.0f, 0.0f),            // lamp cube
        glm::vec3(0.0f, 0.0f, 0.0f)              // reflexive cube
    };
    glm::vec3 &lightPos = cubePositions[0];      // reference to the element
    std::vector<float> cubeScales = {
        0.2,
        1.0
    };
    std::vector<Shader*> shaders = {
        lampShader,
        lightningShader
    };

    // Set up the light scene
    unsigned int lightVAO;
    glGenVertexArrays(1, &lightVAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glm::vec3 lightColor = glm::vec3(1.0f, 1.0f, 1.0f);
    glm::vec3 objColor   = glm::vec3(1.0f, 0.5f, 0.31f);

    // Defining transformations
    glm::mat4 view = glm::mat4(1.0f);
    glm::mat4 proj = glm::mat4(1.0f);
    float lastFrame = 0.0f;

    while (!glHandler->isClosed ())
    {
        // Get current frame time
        float currentTime = glfwGetTime();
        inputHandler->setdt (currentTime - lastFrame);
        lastFrame  = currentTime;

        // Process input
        inputHandler->process();

        // Render the background
        glHandler->renderBackground (backgroundColor);

        // Update the uniforms
        lightPos.x = sin(currentTime);
        lightPos.z = cos(currentTime);

        lightningShader->use();
        lightningShader->setVec3 ("viewPos", camera->getPosition());

/******************************************************************************/
        // Defining the material
        lightningShader->setVec3 ("material.ambient", objColor);
        lightningShader->setVec3 ("material.diffuse", objColor);
        lightningShader->setVec3 ("material.specular", glm::vec3(0.5f));
        lightningShader->setFloat("material.shininess", 32);

        // Defining the light
        lightningShader->setVec3 ("light.position", lightPos);
        lightningShader->setVec3 ("light.ambient",  lightColor * glm::vec3(0.2f));
        lightningShader->setVec3 ("light.diffuse",  lightColor * glm::vec3(0.5f));
        lightningShader->setVec3 ("light.specular", lightColor);
/******************************************************************************/


        // Transform
        proj = glm::perspective(glm::radians(camera->getZoom ()),
                            static_cast<float>(SCR_WIDTH)/static_cast<float>(SCR_HEIGHT),
                            0.1f, 100.0f);

        view = camera->getViewMatrix ();

        glm::mat4 trans = glm::mat4(1.0f);
//        trans = glm::rotate(trans, static_cast<float>(glfwGetTime()), glm::vec3(0.0, 0.0, 1.0));


        for (int i=0; i < cubePositions.size(); i++)
        {
            glm::mat4 model = glm::mat4(1.0f);
//            model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));

            model = glm::translate(model, cubePositions[i]);
            model = glm::scale(model, glm::vec3(cubeScales[i]));
            shaders[i]->use();
            glBindVertexArray(lightVAO);
            shaders[i]->setMatrix4f("model", model);
            shaders[i]->setMatrix4f("view", view);
            shaders[i]->setMatrix4f("proj", proj);
            shaders[i]->setMatrix4f("transform", trans);
            mesh->draw (*shaders[i]);
        }


        // Send it to the GL machinery
//        myShader.use();
//        myShader.setMatrix4f("model", model);
//        myShader.setMatrix4f("view", view);
//        myShader.setMatrix4f("proj", proj);
//        myShader.setMatrix4f("transform", trans);
//        mesh->draw (myShader);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    delete glHandler;
    delete inputHandler;
    delete mesh;

    return 0;
}


/******************************************************************************/

void mouse_callback(GLFWwindow *window, double xpos, double ypos)
{
    if (firstMouse_g)
    {
        lastX_g = xpos;
        lastY_g = ypos;
        firstMouse_g = false;
    }

    float xoffset =   xpos - lastX_g;
    float yoffset = -(ypos - lastY_g);
    lastX_g = xpos;
    lastY_g = ypos;

    camera->processMouseMovement (xoffset, yoffset, MOUSE_CONSTRAINT, INVERT_Y);
}

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset)
{
    camera->processMouseScroll (yoffset);
}
/******************************************************************************/
