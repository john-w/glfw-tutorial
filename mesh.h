#ifndef MESH_H
#define MESH_H

#include "glad/glad.h"

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <string>
#include <vector>

#include "shader.h"
#include "texture2d.h"

using namespace glm;

struct Vertex {
    vec3 position;
    vec3 normal;
    vec2 texCoords;
    vec3 tangent;
    vec3 biTangent;
};

struct Texture {
    unsigned int ID;
    std::string type;
    Texture2D texture;
};

class Mesh
{
    std::vector<Vertex>  _vertices;
    std::vector<unsigned int> _indices;
    std::vector<Texture> _textures;

    unsigned int _VAO;
    unsigned int _VBO;
    unsigned int _EBO;

    private:
        // To be called when constructing
        void setupMesh();

    public:
        Mesh( std::vector<Vertex> vertices,
             std::vector<unsigned int> indices,
             std::vector<Texture> textures);

        void draw(Shader shader);
};

#endif // MESH_H
