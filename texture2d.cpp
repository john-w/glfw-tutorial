#include "texture2d.h"

// Macros... Macros....
// Working in mysterious ways...
// This only works if put on the .cpp instead of the .h
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

Texture2D::Texture2D(char *filename, GLint internalType, bool flip, int texUnit)
    : _fileName(filename), _internalType(internalType), _texUnit(texUnit), _flip(flip)
{
    glGenTextures(1, &_texture);
    glBindTexture(GL_TEXTURE_2D, _texture);

    // Texture filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    generateTexture ();
}

void Texture2D::generateTexture()
{
    int width, height, nrChannels;
    stbi_set_flip_vertically_on_load (_flip);
    unsigned char *data = stbi_load(_fileName, &width, &height, &nrChannels, 0);

    if (data) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, _internalType, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    } else {
        std::cout << "Failed to load texture: " << _fileName << std::endl;
    }

    stbi_image_free(data);
}
