#ifndef TEXTURE2D_H
#define TEXTURE2D_H

#include <iostream>

#include "glad/glad.h"
#include <GLFW/glfw3.h>

class Texture2D
{
    char* _fileName;
    unsigned int _texture;
    GLint _internalType;
    int _texUnit;
    bool _flip;

    public:
        Texture2D(char* filename, GLint internalType, bool flip, int texUnit);
        void generateTexture();
};

#endif // TEXTURE2D_H
