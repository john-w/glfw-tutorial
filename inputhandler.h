#ifndef INPUTHANDLER_HPP
#define INPUTHANDLER_HPP


#include "glhandler.h"
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <cmath>

#include "camera.h"


class InputHandler
{

    bool _wireframe = false;
    float _mixRatio = 0.0f;
    float _deltaTime = 0.0f;

    // GL variables
    GLHandler* _glHandler;
    GLFWwindow *_window;

    Camera *_camera;

    public:
        InputHandler(GLHandler* glhandler, Camera *camera);
        void process();
        void flipSwitch(bool &sw);

    public:
        void setdt(float dt);
        float getMixRatio();
};

#endif // INPUTHANDLER_HPP
