#ifndef CAMERA_H
#define CAMERA_H

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <cmath>

#include <iostream>

#include "constants.h"

using namespace glm;

// camera constants
const float YAW   		= -120.0f;
const float PITCH 		= -25.0f;
const float SPEEDFAC   	=   2.5f;
const float SENSITIVTY 	=   0.1f;
const float ZOOM 		=  45.0f;

const bool INVERT_Y 		= false;
const bool MOUSE_CONSTRAINT	= true;

enum CameraMovement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

class Camera
{
    public:
        Camera(vec3 postion, vec3 up, float yaw, float pitch);

        void updateCameraVectors();
        mat4 getViewMatrix();

    private:
        vec3 _position;
        vec3 _front;
        vec3 _right;
        vec3 _up;
        vec3 _worldUp;

        float _yaw;
        float _pitch;
        float _movementSpeed;
        float _mouseSensitivity;
        float _zoom;

    public:
        void processKeyboard(CameraMovement direction, float dt);
        void processMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch, GLboolean invertY);
        void processMouseScroll(float yoffset);

        float getZoom();
        vec3 getPosition();

};

#endif // CAMERA_H
