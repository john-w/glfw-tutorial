#include "inputhandler.h"


InputHandler::InputHandler(GLHandler *glhandler, Camera *camera)
    : _glHandler(glhandler), _camera(camera)
{
    _window = _glHandler->getWindow ();
}

void InputHandler::process()
{
    if (glfwGetKey(_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(_window, true);
    if (glfwGetKey(_window, GLFW_KEY_SPACE) == GLFW_PRESS) {
        if (_wireframe) {
            flipSwitch (_wireframe);
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        } else {
            flipSwitch (_wireframe);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }
    }
    if (glfwGetKey(_window, GLFW_KEY_UP) == GLFW_PRESS) {
        if (_mixRatio >= 1.0f) {
            _mixRatio = 1.0f;
        } else {
            _mixRatio += 1e-2;
        }
    }
    if (glfwGetKey(_window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        if (_mixRatio <= 0.0f) {
            _mixRatio = 0.0f;
        } else {
            _mixRatio -= 1e-2;
        }
    }

    if (glfwGetKey(_window, GLFW_KEY_W) == GLFW_PRESS)
        _camera->processKeyboard (FORWARD, _deltaTime);
    if (glfwGetKey(_window, GLFW_KEY_S) == GLFW_PRESS)
        _camera->processKeyboard (BACKWARD, _deltaTime);
    if (glfwGetKey(_window, GLFW_KEY_A) == GLFW_PRESS)
        _camera->processKeyboard (LEFT, _deltaTime);
    if (glfwGetKey(_window, GLFW_KEY_D) == GLFW_PRESS)
        _camera->processKeyboard (RIGHT, _deltaTime);


}

void InputHandler::flipSwitch(bool &sw)
{
    sw = !sw;
}

void InputHandler::setdt(float dt)
{
    _deltaTime = dt;
}

float InputHandler::getMixRatio()
{
    return _mixRatio;
}
