#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTexCoords;

out vec3 objColor;
out vec2 TexCoords;

uniform mat4 transform;
uniform mat4 view;
uniform mat4 model;
uniform mat4 proj;

void main()
{
   gl_Position = proj * view * model * transform * vec4(aPos, 1.0);
   objColor = aColor;
   TexCoords = aTexCoords;
}
